#!/bin/sh

date +%s > .ci_status/$1
date +'%Y-%m-%dT%H:%M:%S%:z' > .ci_status/$2
echo $CI_COMMIT_REF_SLUG >> .ci_status/$2
echo $CI_COMMIT_SHA >> .ci_status/$2
#echo "VCS_VERSION:$VCS_VERSION" >> .ci_status/$2
docker inspect $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG --format='{{json .Config.Labels}}' | sed 's/map\[//;s/\]//' | jq  >> .ci_status/$2
git log --decorate --pretty=format:'{%n "commit": "%H",%n "branch": "%d",%n "author": "%an <%ae>",%n "date": "%ad",%n "message": "%s":%n},' | sed 's/\"%s\"/\"%f\"/g' > .ci_status/$3